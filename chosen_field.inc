<?php

/**
 * @file
 * Chosen Field include file
 */

/**
 * Get path to Chosen js file
 *
 * @return bool|string
 */
function chosen_field_get_js_path() {
  $dir = libraries_get_path('chosen');

  // Use the minified file if it exists to preserve bandwidth.
  if (file_exists($dir . '/chosen.jquery.min.js')) {
    $path = $dir . '/chosen.jquery.min.js';
  }
  elseif (file_exists($dir . '/chosen.jquery.js')) {
    $path = $dir . '/chosen.jquery.js';
  }

  return (isset($path)) ? $path : FALSE;
}

/**
 * Includes required JavaScript and CSS files to the page.
 */
function chosen_field_load_assets() {
  $dir = libraries_get_path('chosen');

  drupal_add_css($dir . '/chosen.css');
  drupal_add_js(chosen_field_get_js_path());
  drupal_add_js(drupal_get_path('module', 'chosen_field') . '/chosen_field.js');
}