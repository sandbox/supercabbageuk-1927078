CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Usage

INTRODUCTION
------------

Current Maintainer: supercabbageuk

Chosen Field adds a field widget type to the text (list) and term reference fields

INSTALLATION
------------

1. Install the module as standard.
2. Download Chosen from http://harvesthq.github.com/chosen/ and extract the contents of the file to sites/all/libraries/chosen

USAGE
=====

You will be able to choose the Chosen widget for any text (list) or term reference fields
